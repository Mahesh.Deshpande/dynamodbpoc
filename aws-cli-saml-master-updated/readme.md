# For internal cloud team use only. See the [devleoper onboarding](https://confluence.int.ally.com/x/7gkBBw) documentation in Confluence for instructions to generate AWS credentials on your own system

# Table of Contents
1. [About](#About)
2. [Prerequisite](#Prerequisite)
3. [SAML](#SAML)
4. [AssumeRole](#AssumeRole)
## About
Script to request AWS Tempory Credentials/Token using SAML and MFA (if enabled). This script will automatically update your `.aws/credentials` file.
## Prerequisite
* Python 3.x installed (pip)
  * Python dependencies installed (boto3, requests, beatuifulsoup4, configparser)
    ```` Console
    pip install --trusted-host pypi.org --trusted-host files.pythonhosted.org --proxy=${https_proxy} boto3
    pip install --trusted-host pypi.org --trusted-host files.pythonhosted.org --proxy=${https_proxy} requests
    pip install --trusted-host pypi.org --trusted-host files.pythonhosted.org --proxy=${https_proxy} beautifulsoup4
    pip install --trusted-host pypi.org --trusted-host files.pythonhosted.org --proxy=${https_proxy} configparser
    ````
  * MacOs Python 3 is likely mapped to 'python3' and pip to 'pip3'.  Must use "--user" flag
    ```` Console
    pip3 install --trusted-host pypi.org --trusted-host files.pythonhosted.org --proxy=${https_proxy} boto3 --user
    pip3 install --trusted-host pypi.org --trusted-host files.pythonhosted.org --proxy=${https_proxy} requests --user
    pip3 install --trusted-host pypi.org --trusted-host files.pythonhosted.org --proxy=${https_proxy} beautifulsoup4 --user
    pip3 install --trusted-host pypi.org --trusted-host files.pythonhosted.org --proxy=${https_proxy} configparser --user
    ````
* Possible proxy configuration
  * Replace `<USERNAME>` and `<PASSWORD>` with your non-privileged credentials respectively
    ```` Console
    #########################
    # mac
    #########################
    # Excellent resource for proxy configurations:
    # https://bitbucket.int.ally.com/projects/AWEB/repos/setup-script/browse
    # Set the https_proxy url
    export http_proxy="http://<USERNAME>:<PASSWORD>@10.43.196.154:80"
    export https_proxy="http://<USERNAME>:<PASSWORD>@10.43.196.154:80"
    #########################
    # Windows
    #########################
    # Set the https_proxy url
    $env:HTTP_PROXY = "http://<USERNAME>:<PASSWORD>@10.43.196.154:80"
    [Environment]::SetEnvironmentVariable("HTTP_PROXY", "http://<USERNAME>:<PASSWORD>@10.43.196.154:80", "User")
    $env:HTTPS_PROXY = "http://<USERNAME>:<PASSWORD>@10.43.196.154:80"
    [Environment]::SetEnvironmentVariable("HTTPS_PROXY", "http://<USERNAME>:<PASSWORD>@10.43.196.154:80", "User")
    ````
## SAML
1. run script with `python main.py` - MacOs `python3 main.py`
    * Optional arguments
        * `--profile <profile>` AWS profile to save credentials to
        * `--duration <seconds>` Duration in Seconds to keep credentials alive
2. Enter account ID with domain prefix EX: `nao\<id>`
3. Follow additional prompts for MFA and role selection
4. Credentials will automatically be written to your aws credentials file under the `saml` profile unless specified
5. Cheers!
## AssumeRole
1. run script with `python assume-role.py --account <AWS Account #> --role <AWS Role Name>`
    * Optional arguments
        * `--execution-profile <profile>` Profile to execute the AssumeRole API call with. Default is saml.
        * `--assume-role-profile <profile>` Profile to save the AssumeRole credentials too. Default is assumerole.
2. Profile will automatically be written to your `.aws/credentials` file.
