package com.example.dynamodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DynamodbpocApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamodbpocApplication.class, args);
		UserDetailsRepositoryV2 dao = new UserDetailsRepositoryV2();
		UserDetails userDetail = new UserDetails();
		userDetail.setUserId("250");
		userDetail.setLastName("Deshpande");
		userDetail.setFirstName("Mahesh");
		dao.addUserDetails(userDetail);
	}

}



